# Lingowords site
---
Deelsysteem 2 voor het spelen van het lingo spel

## Releases
 - [lingo.robert-jan.nl](http://lingo.robert-jan.nl/)

## Setup
```
$ composer install
$ python3 main.py
```

## Tests 
```
$ python -m unittest discover tests
```

## Static analysis
```
$ python3 -m pyflakes .
```

## Coverage
```
$ coverage run --source=lingo_words -m unittest discover
$ coverage report
```

## Coverage result:
De coverage results zijn te bekijken in de [build](https://travis-ci.com/KeizerDev/lingowords-bot).
