<?php

namespace App\Utils;


class LingoHelperTest extends \PHPUnit_Framework_TestCase
{

    public function testGetCorrectWordHint()
    {
        $correct_word_hint = LingoHelper::getCorrectWordHint("testen");
        $this->assertContains(["letter" => "t", "result" => GuessType::Correct], $correct_word_hint);
        $this->assertContains(["letter" => "e", "result" => GuessType::Correct], $correct_word_hint);
        $this->assertContains(["letter" => LingoHelper::FILL_CHARACTER, "result" => GuessType::Wrong], $correct_word_hint);
    }

    public function testGetGuessResultsByCorrectWord()
    {
        $guess_results = LingoHelper::getGuessResultsByCorrectWord("testen", "tested");
        $this->assertNotContains(["letter" => "e", "result" => GuessType::WrongPosition], $guess_results);
        $this->assertNotContains(["letter" => "e", "result" => GuessType::Wrong], $guess_results);
        $this->assertContains(["letter" => "e", "result" => GuessType::Correct], $guess_results);
        $this->assertCount(6, $guess_results);
    }
}
