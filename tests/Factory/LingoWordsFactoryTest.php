<?php

namespace App\Factory;


use App\Model\Game;
use App\Model\Guess;
use App\Model\Round;
use App\Utils\LingoHelper;
use Illuminate\Database\Capsule\Manager;
use Symfony\Component\Yaml\Yaml;

class LingoWordsFactoryTest extends \PHPUnit_Framework_TestCase
{

    protected function setUp()
    {
        $this->configureDatabase();
    }

    public function testCreateGuess()
    {
        $round = Round::first();
        $lingowordsFactory = new LingoWordsFactory();
        $guess = $lingowordsFactory->createGuess($round, "newgues");
        $this->assertInstanceOf(Guess::class, $guess);
    }

    public function testCreateGame()
    {
        $lingowordsFactory = new LingoWordsFactory();
        $game_id = LingoHelper::generateGameId();
        $game = $lingowordsFactory->createGame($game_id);
        $this->assertInstanceOf(Game::class, $game);
    }

    public function testCreateRound()
    {
        $lingowordsFactory = new LingoWordsFactory();
        $game = Game::first();

        $round = $lingowordsFactory->createRound($game, "testen", 6, 5);
        $this->assertInstanceOf(Round::class, $round);
    }

    private function configureDatabase()
    {
        $capsule = new Manager;
        $capsule->addConnection([
            "driver" => "sqlite",
            "database" => __DIR__ . "/../../data/data.db",
            "charset" => "utf8",
            "collation" => "utf8_unicode_ci",
            "prefix" => "",
        ]);

        $capsule->addConnection([
            "driver" => "sqlite",
            "database" => __DIR__ . "/../../resources/words.db",
            "charset" => "utf8",
            "collation" => "utf8_unicode_ci",
            "prefix" => "",
        ], "words");
        $capsule->setAsGlobal();
        $capsule->bootEloquent();
    }
}
