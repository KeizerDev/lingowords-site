<?php

use Symfony\Component\Yaml\Yaml;

if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

require __DIR__ . '/../vendor/autoload.php';

$settings = Yaml::parseFile(__DIR__ . "/../config.yml");

$app = new \Slim\App($settings);

require __DIR__ . '/../src/App/dependencies.php';
require __DIR__ . '/../src/App/middleware.php';

/*
 * Routes
 * */
$app->get('/', 'MainController:renderMainPage')->setName('home');

$app->get('/lingo', 'LingoController:renderNewGamePage')->setName('lingo.new_game');
$app->get('/lingo/{game_id}', 'LingoController:renderGamePage')->setName('lingo.game');
$app->post('/lingo/{game_id}/handle_answer', 'LingoController:handleNewGuess')->setName('lingo.game_guess');
$app->get('/lingo/{game_id}/game_over', 'LingoController:renderGameOverPage')->setName('lingo.game_over');

$app->get('/highscore', 'HighScoreController:renderHighScorePage')->setName('highscore.all');

// Run app
$app->run();
