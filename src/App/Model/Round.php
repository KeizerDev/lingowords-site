<?php


namespace App\Model;


use App\Utils\LingoHelper;
use Illuminate\Database\Eloquent\Model;

class Round extends Model
{
    protected $table = 'round';
    public $primaryKey = 'id';

    protected $fillable = [
        'game_id',
        'correct_word',
        'correct_word_length',
        'maximum_guesses'
    ];

    protected $casts = [
        'is_finished' => 'boolean',
        'correct_word_length' => 'int',
        'maximum_guesses' => 'int',
    ];


    public function active($query)
    {
        return $query->where('is_finished', 0);
    }

    public function scopeLatest($query)
    {
        return $query->orderBy('id', 'DESC');
    }

    public function guesses()
    {
        return $this->hasMany(Guess::class, 'round_id');
    }

    public function amountOfGuesses()
    {
        return count($this->guesses()->get());
    }

    public function exceededGuessLimit()
    {
        return $this->amountOfGuesses() >= $this->maximum_guesses;
    }

    public function getHint()
    {
        return LingoHelper::getCorrectWordHint($this->correct_word);
    }
}
