<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $table = 'game';
    public $primaryKey = 'id';

    protected $fillable = [
        'game_id',
        'game_name',
        'is_game_finished',
    ];

    protected $casts = [
        'is_game_finished' => 'boolean',
    ];

    public function rounds()
    {
        return $this->hasMany(Round::class, 'game_id', 'game_id');
    }

    public function getActiveRound()
    {
        return $this->rounds->where('is_finished', 0)->first();
    }

    public function getGameNameAttribute()
    {
        return $this->game_name ?? "-";
    }

    public function getIsGameOverAttribute()
    {
        return $this->is_game_finished;
    }

    public function getNumberOfRounds()
    {
        return count($this->rounds()->get());
    }

    public function getScore()
    {
        return $this->getNumberOfRounds();
    }

    public function scopeHighScores($query)
    {
        return $query->where('is_game_finished', '1');
    }

}
