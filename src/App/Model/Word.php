<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Word extends Model
{
    protected $connection = "words";

    protected $table = 'word';
    public $primaryKey = 'id';

    protected $fillable = [
        'word',
        'word_length',
    ];

    protected $casts = [
        'word_length' => 'int',
    ];

    public static function randomWord()
    {
        return self::all()->random();
    }
}
