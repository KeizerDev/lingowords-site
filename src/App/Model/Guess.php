<?php


namespace App\Model;


use App\Utils\LingoHelper;
use Illuminate\Database\Eloquent\Model;

class Guess extends Model
{
    protected $table = 'guess';
    public $primaryKey = 'id';
    public $timestamps = null;

    protected $fillable = [
        'round_id',
        'guess',
    ];

    protected $casts = [
        'is_correct' => 'boolean',
    ];

    public function round()
    {
        return $this->hasOne(Round::class, 'id', 'round_id');
    }

    public function getLingoResult()
    {
        $round = $this->round;
        $correct_word = $round->correct_word;

        return LingoHelper::getGuessResultsByCorrectWord($this->guess, $correct_word);
    }

    public function getIsCorrectGuessAttribute()
    {
        return $this->guess == $this->round->correct_word;
    }
}
