<?php

namespace App\Controller;

use Slim\Http\Request;
use Slim\Http\Response;

class MainController extends BaseController
{
    public function renderMainPage(Request $request, Response $response, $args)
    {
        return $this->view->render($response, "index.html.twig", []);
    }
}
