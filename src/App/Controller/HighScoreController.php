<?php

namespace App\Controller;

use Slim\Http\Request;
use Slim\Http\Response;

class HighScoreController extends BaseController
{
    public function renderHighScorePage(Request $request, Response $response, $args)
    {
        return $this->view->render($response, "highscores.html.twig", [
            "game_highscores" => $this->lingoService->getGameHighScores()
        ]);
    }
}
