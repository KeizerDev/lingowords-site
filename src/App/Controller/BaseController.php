<?php

namespace App\Controller;

use App\Service\LingoService;
use Slim\Router;
use Slim\Views\Twig;
use Symfony\Component\Form\FormFactoryInterface;

abstract class BaseController
{
    /* @var Twig $view */
    protected $view;

    /* @var LingoService $lingoService */
    protected $lingoService;

    /** @var FormFactoryInterface $formFactory */
    protected $formFactory;

    /** @var Router $router */
    protected $router;

    public function __construct($view, $lingoService, $formFactory, $router)
    {
        $this->view = $view;
        $this->formFactory = $formFactory;
        $this->lingoService = $lingoService;
        $this->router = $router;
    }
}
