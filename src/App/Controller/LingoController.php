<?php

namespace App\Controller;

use App\Forms\LingoTextType;
use Slim\Http\Request;
use Slim\Http\Response;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class LingoController extends BaseController
{
    public function renderNewGamePage(Request $request, Response $response, $args)
    {
        $game = $this->lingoService->startNewGame();

        return $response->withRedirect(
            $this->router->pathFor('lingo.game', [
                'game_id' => $game->game_id
            ])
        );
    }

    public function renderGamePage(Request $request, Response $response, $args)
    {
        $game_id = $args['game_id'];

        $game = $this->lingoService->getGameUpdatesByGameId($game_id);

        if ($game->isGameOver) {
            return $response->withRedirect(
                $this->router->pathFor('lingo.game_over', [
                    'game_id' => $game_id
                ])
            );
        }

        $lingo_form = $this->getLingoForm($game_id, $this->lingoService->getWordLengthByGame($game));

        print_r($this->lingoService->getCorrectWordByGame($game));
        return $this->view->render($response, "lingo/game.html.twig", [
            "game" => $game,
            "form" => $lingo_form->createView()
        ]);
    }

    public function handleNewGuess(Request $request, Response $response, $args)
    {
        $game_id = $args['game_id'];
        $game = $this->lingoService->getGameByGameId($game_id);

        $lingo_form = $this->getLingoForm($game_id, $this->lingoService->getWordLengthByGame($game));

        $results = $request->getParsedBody();
        if (isset($results)) {
            $results = $results['form'];
        }
        $lingo_form->submit($results);

        if ($lingo_form->isSubmitted() && $lingo_form->isValid()) {
            $data = $lingo_form->getData();
            $new_guess = strtolower($data['guess']);

            $game = $this->lingoService->handleGuessForGame($new_guess, $game);

            return $response->withRedirect(
                $this->router->pathFor('lingo.game', [
                    'game_id' => $game->game_id
                ])
            );
        }

        return $this->view->render($response, "lingo/game.html.twig", [
            "game" => $game,
            "form" => $lingo_form->createView()
        ]);
    }

    public function renderGameOverPage(Request $request, Response $response, $args)
    {
        $game_id = $args['game_id'];
        $game = $this->lingoService->getGameByGameId($game_id);

        if (!$game->isGameOver) {
            return $response->withRedirect(
                $this->router->pathFor('home', [])
            );
        }

        return $this->view->render($response, "lingo/game_over.html.twig", [
            "score" => $game->getScore(),
            "correct_word" => $this->lingoService->getCorrectWordByGame($game)
        ]);
    }

    public function getLingoForm($game_id, $text_length)
    {
        $guess_post_url = $this->router->pathFor('lingo.game_guess', ['game_id' => $game_id]);

        $form = $this->formFactory->createBuilder()
            ->setAction($guess_post_url)
            ->add('guess', LingoTextType::class, [
                'constraints' => [
                    new NotBlank(),
                    new Length(['min' => $text_length, 'max' => $text_length]),
                ],
                'attr' => [
                    'autofocus' => true
                ],
                'data' => '',
                'help' => "Voor een $text_length letterig woord in"
            ])->getForm();

        return $form;
    }
}
