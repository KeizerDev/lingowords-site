<?php

namespace App\Utils;

class LingoHelper
{
    const FILL_CHARACTER = ".";
    const HINT_SIZE = 1;

    public static function getGuessResultsByCorrectWord($guess, $correct_word): array
    {
        $results = [];

        $guess_arr = self::_stringToArray($guess);
        $correct_word_arr = self::_stringToArray($correct_word);
        $correct_chars = $correct_word_arr;


        foreach ($guess_arr as $guess_index => $guess_char) {
            if ($correct_word_arr[$guess_index] == $guess_char) {
                $guess_type = GuessType::Correct;

                // Remove the character from the correct_chars
                $correct_chars[$guess_index] = self::FILL_CHARACTER;
            } else {
                $guess_type = GuessType::Wrong;
            }

            $results[$guess_index] = self::guessResult($guess_char, $guess_type);

        }

        foreach ($guess_arr as $guess_index => $guess_char) {
            if (in_array($guess_char, $correct_chars)) {
                foreach ($correct_chars as $char_i => $char) {
                    if ($char == $guess_char) {
                        unset($correct_chars[$char_i]);
                        continue;
                    }
                }
                $results[$guess_index] = self::guessResult($guess_char, GuessType::WrongPosition);
                continue;
            }
        }

        return $results;
    }

    public static function getCorrectWordHint($correct_word): array
    {
        $hint_results = [];

        $correct_word_arr = self::_stringToArray($correct_word);
        foreach ($correct_word_arr as $correct_word_index => $correct_word_char) {
            if ($correct_word_index <= self::HINT_SIZE) {
                $hint_results[] = self::guessResult($correct_word_char, GuessType::Correct);
                continue;
            }
            $hint_results[] = self::guessResult(self::FILL_CHARACTER, GuessType::Wrong);
        }

        return $hint_results;

    }

    static function guessResult($char, $result)
    {
        return ["letter" => $char, "result" => $result];
    }

    private static function _stringToArray($s)
    {
        $r = array();
        for ($i = 0; $i < strlen($s); $i++)
            $r[$i] = $s[$i];
        return $r;
    }


    /**
     * Got this from stackoverflow, source.
     * @link https://stackoverflow.com/questions/4356289/php-random-string-generator
     *
     * @param int $length
     * @return string
     */
    private static function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function generateGameId()
    {
        return self::generateRandomString();
    }
}

abstract class GuessType
{
    const Correct = 0;
    const WrongPosition = 1;
    const Wrong = 2;
}
