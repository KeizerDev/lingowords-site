<?php

namespace App\Repository;


use App\Factory\LingoWordsFactory;
use App\Model\Game;
use App\Model\Round;
use App\Model\Word;
use App\Utils\LingoHelper;
use phpDocumentor\Reflection\Types\String_;

class LingoWordsRepository implements ILingoWordsRespository
{
    const MAXIMUM_GUESSES = 5;

    /**
     * @var LingoWordsFactory
     */
    private $lingoWordsFactory;


    public function __construct(LingoWordsFactory $lingoWordsFactory)
    {
        $this->lingoWordsFactory = $lingoWordsFactory;
    }

    public function startNewGame(): Game
    {
        $game_id = LingoHelper::generateGameId();
        $game = $this->lingoWordsFactory->createGame($game_id);
        return $this->handleGameUpdates($game);
    }

    /**
     * @param Game $game
     * @return Game
     */
    public function handleGameUpdates(Game $game): Game
    {
        if ($game->isGameOver) return $game;

        $active_round = $game->getActiveRound();

        if (!$active_round) {
            $active_round = $this->newRound($game);
        }

        if ($active_round->exceededGuessLimit()) {
            $game = $this->finishGame($game);
        }

        return $game;

    }

    public function getGameHighScores()
    {
        return Game::highscores()->get();
    }

    public function getWordLengthByRound(Round $round): int
    {
        return $round->correct_word_length;
    }

    public function getCorrectWordByRound(Round $round): string
    {
        return $round->correct_word;
    }


    public function getGameByGameId($game_id): Game
    {
        return Game::where('game_id', $game_id)->firstOrFail();
    }

    public function setGameFinishedByGameId($game_id)
    {
        $game = $this->getGameByGameId($game_id);
        $game->is_game_finished = true;
        $game->save();
    }

    public function finishGame($game)
    {
        $game->is_game_finished = true;
        $game->save();
        return $game;
    }


    public function guessIsCorrect($guess)
    {
        $guess->is_correct = true;
        $guess->save();

    }

    public function roundIsFinished($round)
    {
        $round->is_finished = true;
        $round->save();
    }

    public function handleGuessForGame($guess, $game): Game
    {
        $round = $game->getActiveRound();
        $guess = $this->lingoWordsFactory->createGuess($round, $guess);

        if ($guess->isCorrectGuess) {
            $this->guessIsCorrect($guess);
            $this->roundIsFinished($round);
        }
        return $game;
    }

    public function getActiveRoundByGame(Game $game): Round
    {
        return $game->getActiveRound();
    }

    private function newRound(Game $game)
    {
        $random_word = $this->generateRandomWord();

        $word = $random_word->word;
        $word_length = $random_word->word_length;

        return $this->lingoWordsFactory->createRound($game, $word, $word_length, self::MAXIMUM_GUESSES);
    }

    private function generateRandomWord(): Word
    {
        return Word::randomWord();
    }
}
