<?php


namespace App\Repository;

use App\Model\Game;
use App\Model\Round;

interface ILingoWordsRespository
{

    public function getGameHighScores();

    public function getGameByGameId($game_id): Game;

    public function setGameFinishedByGameId($game_id);

    public function startNewGame(): Game;

    public function handleGameUpdates(Game $game): Game;

    public function handleGuessForGame($new_guess, $game): Game;

    public function getActiveRoundByGame(Game $game): Round;

    public function getWordLengthByRound(Round $round): int;

    public function getCorrectWordByRound(Round $round): string;
}
