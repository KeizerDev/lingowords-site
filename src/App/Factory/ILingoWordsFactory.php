<?php


namespace App\Factory;


use App\Model\Game;
use App\Model\Guess;
use App\Model\Round;

interface ILingoWordsFactory
{
    public function createGame($game_id): Game;

    public function createRound(Game $game, string $correct_word, int $correct_word_length, int $maximum_guesses): Round;

    public function createGuess(Round $round, string $guessed_value): Guess;
}
