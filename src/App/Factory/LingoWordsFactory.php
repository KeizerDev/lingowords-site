<?php


namespace App\Factory;


use App\Model\Game;
use App\Model\Guess;
use App\Model\Round;
use phpDocumentor\Reflection\Types\Integer;

class LingoWordsFactory implements ILingoWordsFactory
{

    public function createGame($game_id): Game
    {
        $game = new Game();
        $game->game_id = $game_id;
        $game->save();

        return $game;
    }

    public function createRound(Game $game, string $correct_word, int $correct_word_length, int $maximum_guesses): Round
    {
        $round = new Round();
        $round->game_id = $game->game_id;
        $round->correct_word = $correct_word;
        $round->correct_word_length = $correct_word_length;
        $round->maximum_guesses = $maximum_guesses;
        $round->save();

        return $round;
    }

    public function createGuess(Round $round, string $guessed_value): Guess
    {
        $guess = new Guess();
        $guess->round_id = $round->id;
        $guess->guess = $guessed_value;
        $guess->save();

        return $guess;
    }

}
