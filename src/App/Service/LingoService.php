<?php

namespace App\Service;

use App\Model\Game;
use App\Repository\ILingoWordsRespository;

class LingoService
{
    /**
     * @var ILingoWordsRespository
     */
    private $lingoWordsRepository;


    public function __construct(ILingoWordsRespository $lingoWordsRespository)
    {
        $this->lingoWordsRepository = $lingoWordsRespository;
    }

    public function getGameHighScores()
    {
        return $this->lingoWordsRepository->getGameHighScores();
    }

    public function startNewGame(): Game
    {
        return $this->lingoWordsRepository->startNewGame();
    }

    public function getGameByGameId($game_id)
    {
        return $this->lingoWordsRepository->getGameByGameId($game_id);
    }

    public function getGameUpdates(Game $game): Game
    {
        return $this->lingoWordsRepository->handleGameUpdates($game);
    }

    public function getGameUpdatesByGameId($game_id): Game
    {
        $game = $this->getGameByGameId($game_id);
        return $this->getGameUpdates($game);
    }

    public function handleGuessForGame($new_guess, $game): Game
    {
        $this->lingoWordsRepository->handleGuessForGame($new_guess, $game);
        return $this->getGameUpdates($game);
    }


    public function getWordLengthByGame(Game $game)
    {
        $active_round = $this->lingoWordsRepository->getActiveRoundByGame($game);

        return $this->lingoWordsRepository->getWordLengthByRound($active_round);
    }

    public function getCorrectWordByGame(Game $game)
    {
        $active_round = $this->lingoWordsRepository->getActiveRoundByGame($game);
        return $this->lingoWordsRepository->getCorrectWordByRound($active_round);
    }

}
