<?php
// DIC configuration
use App\Controller\HighScoreController;
use App\Controller\LingoController;
use App\Controller\MainController;
use App\Factory\LingoWordsFactory;
use App\Repository\LingoWordsRepository;
use App\Service\LingoService;
use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;
use Slim\Views\TwigExtension;
use Symfony\Bridge\Twig\Extension\FormExtension;
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Symfony\Bridge\Twig\Form\TwigRendererEngine;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\FormRenderer;
use Symfony\Component\Form\Forms;
use Symfony\Component\Validator\Validation;
use Twig\RuntimeLoader\FactoryRuntimeLoader;

$container = $app->getContainer();

$container['forms'] = function () {
    $validator = Validation::createValidator();
    return Forms::createFormFactoryBuilder()
        ->addExtension(new ValidatorExtension($validator))
        ->getFormFactory();

};

// Twig
$container['view'] = function ($container) {
    $settings = $container->get('settings')['twig'];

    $view = new Twig($settings['templates'], [
        'cache' => $settings['cache']
    ]);

    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new TwigExtension($container['router'], $basePath));
    $view->getEnvironment()->addGlobal('site', $container->get('settings')['site']);


    $view->getEnvironment()->addExtension(new TranslationExtension());

    $formEngine = new TwigRendererEngine(['layouts/forms/bootstrap_4_layout.html.twig'], $view->getEnvironment());
    $view->getEnvironment()->addRuntimeLoader(new FactoryRuntimeLoader([
        FormRenderer::class => function () use ($formEngine) {
            return new FormRenderer($formEngine);
        },
    ]));
    $view->getEnvironment()->addExtension(new FormExtension());


    return $view;
};

$container['FormFactory'] = function ($container) {
    return $container->get('forms');
};

$container['LingoWordsFactory'] = function () {
    return new LingoWordsFactory();
};

$container['LingoWordsRepository'] = function ($container) {
    return new LingoWordsRepository($container['LingoWordsFactory']);
};

/**
 * Boot Eloquent
 * */
$capsule = new Manager;
$capsule->addConnection($container->get('settings')['eloquent']['db']);


$capsule->addConnection($container->get('settings')['eloquent']['words'], "words");
$capsule->setAsGlobal();
$capsule->bootEloquent();

$container['LingoService'] = function ($container) {
    return new LingoService(
        $container->get('LingoWordsRepository')
    );
};

/**
 * Controllers
 * */
$container['MainController'] = function ($container) {
    return new MainController(
        $container->get('view'),
        $container->get('LingoService'),
        $container->get('FormFactory'),
        $container->get('router')
    );
};

$container['LingoController'] = function ($container) {
    return new LingoController(
        $container->get('view'),
        $container->get('LingoService'),
        $container->get('FormFactory'),
        $container->get('router')
    );
};

$container['HighScoreController'] = function ($container) {
    return new HighScoreController(
        $container->get('view'),
        $container->get('LingoService'),
        $container->get('FormFactory'),
        $container->get('router')
    );
};

$container['notFoundHandler'] = function ($c) {
    return function (Request $request, Response $response) use ($c) {
        $view = $c->get('view');
        $view->render($response, '404.html.twig');
        return $response;
    };
};

$container['errorHandler'] = function ($c) {
    return function ($request, $response, $exception) use ($c) {
        if ($exception instanceof ModelNotFoundException) {
            $view = $c->get('view');
            $view->render($response, '404.html.twig');
            return $response->withStatus(404);
        }

        return $response->withStatus(500)
            ->withHeader('Content-Type', 'text/html')
            ->write($exception->getMessage());
    };
};
